package ru.geekbrains.java2_lesson_3.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

class ChatWindow extends JFrame {
    private final int PORT = 8189;
    private final String SERVER_IP = "localhost";
    private JTextArea jTextArea;
    private JTextArea jtaUsers;
    private JScrollPane jspUsers;
    private JTextField jTextField;
    private JTextField jtfLogin;
    private JPasswordField jtfPassword;
    private JPanel bottom, top;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private boolean isAuthorized;

    ChatWindow() {
        setTitle("Chat Window");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400, 400);
        setLocationRelativeTo(null);
        jTextArea = new JTextArea(); //выводится история
        jtaUsers = new JTextArea();
        jtaUsers.setEditable(false);
        jtaUsers.setPreferredSize(new Dimension(150, 1));
        jspUsers = new JScrollPane(jtaUsers);
        jTextArea.setEditable(false);
        jTextArea.setLineWrap(true);
        JScrollPane jScrollPane = new JScrollPane(jTextArea);
        jTextField = new JTextField(); //окно для ввода текста
        jTextField.setPreferredSize(new Dimension(200, 20));
        bottom = new JPanel();
        JButton jButtonSend = new JButton("Send");
        bottom.add(jTextField, BorderLayout.CENTER);
        bottom.add(jButtonSend, BorderLayout.EAST);
        jtfLogin = new JTextField();
        jtfPassword = new JPasswordField();
        JButton jbAuth = new JButton("Login");
        top = new JPanel(new GridLayout(1, 3));
        top.add(jtfLogin);
        top.add(jtfPassword);
        top.add(jbAuth);

        add(jspUsers, BorderLayout.EAST);
        add(jScrollPane, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);
        add(top, BorderLayout.NORTH);

        //LISTNERS
        jButtonSend.addActionListener(e -> sendMessage());
        jTextField.addActionListener(e -> sendMessage());
        jbAuth.addActionListener(e -> auth());
        addWindowListener(new WindowAdapter() { //отвечает за закрытие соединения при закрытии окна через крестик
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    out.writeUTF("/end");
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                    setAuthorized(false);
                }
            }
        });
        start();
        setAuthorized(false);
//        Timer timer1 = new Timer(12000, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if(!isAuthorized){
//                    try {
//                        socket.close();
//                        jTextArea.append("socket close"+"\n");
//                    } catch (IOException e1) {
//                        e1.printStackTrace();
//                    }
//                }
//            }
//        });
//        timer1.start();

        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(50000);
                    if (!isAuthorized) {
                        try {
                            socket.close();
                            jTextArea.append("socket close" + "\n");
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        setVisible(true);
    }

    private void setAuthorized(boolean authorized) { //скрываем панели для авторизованых и неавторизованых пользователей
        isAuthorized = authorized;
        top.setVisible(!isAuthorized);
        bottom.setVisible(isAuthorized);
        jspUsers.setVisible(isAuthorized);
    }

    //LOGIC
    private void start() {
        try {
            socket = new Socket(SERVER_IP, PORT);
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread thread1 = new Thread(() -> {
            try {
                while (true) {
                    String msg = in.readUTF();
                    if (msg.startsWith("/authok")) {
                        setAuthorized(true);
                        break;
                    }


                    jTextArea.append(msg + "\n");
                    jTextArea.setCaretPosition(jTextArea.getDocument().getLength());
                }
                while (true) {
                    String msg = in.readUTF();
                    if (msg.startsWith("/")) {
                        if (msg.startsWith("/userslist")) {
                            String[] users = msg.split(" ");
                            jtaUsers.setText("");
                            for (int i = 1; i < users.length; i++) {
                                jtaUsers.append(users[i] + "\n");
                            }
                        }
                    } else {
                        jTextArea.append(msg + "\n");
                        jTextArea.setCaretPosition(jTextArea.getDocument().getLength()); //перемещает курсор на самый последний символ чата
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                setAuthorized(false);
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();


    }

    private void auth() {
        if (socket == null || socket.isClosed()) start();
        try {
            out.writeUTF("/auth " + jtfLogin.getText() + " " + jtfPassword.getText());
            jtfLogin.setText("");
            jtfPassword.setText("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage() {
        String msg = jTextField.getText();
        jTextField.setText("");
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
