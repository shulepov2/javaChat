package ru.geekbrains.java2_lesson_3.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler {
    private Server server;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private String name;

    ClientHandler(Socket socket, Server server) {
        try {
            this.server = server;
            this.socket = socket;
            name = "undefined";
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            try {
                //Авторизация
                while (true) {
                    String msg = in.readUTF();
                    if (msg.startsWith("/auth")) {
                        String[] elements = msg.split(" ");
                        String nick = server.getAuthService().getNickByLoginPass(elements[1], elements[2]);
                        System.out.println(nick);
                        if (nick != null) { // если пользователь указал правильные логин/пароль
                            if (!server.isNickBusy(nick)) {
                                sendMessage("/authok " + nick);
                                this.name = nick;
                                server.broadcastUsersList();
                                server.broadcast(this.name + " зашел в чат");
                                break;
                            } else sendMessage("Учетная запись уже используется");
                        } else sendMessage("Не верные логин/пароль");
                    } else sendMessage("Для начала надо авторизоваться!");
                } //пока не прервется цикл авторизации, не начнется цикл приема сообщений
                while (true) {
                    String msg = in.readUTF();
                    System.out.println("client: " + msg);
                    if (msg.startsWith("/")) {
                        if (msg.equalsIgnoreCase("/end")) break;
                        else if (msg.startsWith("/w")) { //w nick message
                            String nameTo = msg.split(" ")[1];
                            String message = msg.substring(4 + nameTo.length()); //4 = / + w + 2 пробела (до ника и после)
                            server.sendPrivateMessage(this, nameTo, message);
                        } else sendMessage("Такой команды нет!");
                    } else {
                        server.broadcast(this.name + " " + msg);
                    }
                    //                        sendMessage("echo: " + msg);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                server.unSubscribeMe(this);
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public String getName() {
        return name;
    }

    public void sendMessage(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
