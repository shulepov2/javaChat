package ru.geekbrains.java2_lesson_3.server;

public interface AuthService {
    void start();

    void stop();

    String getNickByLoginPass(String login, String pass);
}
