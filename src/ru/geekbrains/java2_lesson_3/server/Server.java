package ru.geekbrains.java2_lesson_3.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class Server {
    private final int PORT = 8189;
    private Vector<ClientHandler> clients; //вектор - это потокобезопасная коллекция
    private AuthService authService;

    Server() {
        ServerSocket serverSocket = null;
        Socket socket = null;
        clients = new Vector<>();
        try {
            serverSocket = new ServerSocket(PORT);
            authService = new BaseAuthService();
            authService.start(); //пока что пустой! Заглушка
            System.out.println("Сервер запущен, ждем клиентов");
            while (true) {
                socket = serverSocket.accept();
                clients.add(new ClientHandler(socket, this));
                System.out.println("Клиент подключился");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (serverSocket != null) {
                    serverSocket.close();
                }
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            authService.stop(); //заглушка
        }
    }

    public AuthService getAuthService() {
        return authService;
    }

    public boolean isNickBusy(String nick) {
        System.out.println(nick);
        for (ClientHandler c : clients) {
            if (c.getName().equals(nick)) return true;
        }
        return false;
    }

    public void broadcast(String msg) {
        for (ClientHandler c : clients) {
            c.sendMessage(msg);
        }
    }

    public void broadcastUsersList() {
        StringBuilder sb = new StringBuilder("/userslist");
        for (ClientHandler c : clients) {
            sb.append(" ").append(c.getName());
        }
        for (ClientHandler c : clients) {
            c.sendMessage(sb.toString());
        }
    }

    public void sendPrivateMessage(ClientHandler from, String to, String msg) {
        for (ClientHandler c : clients) {
            if (c.getName().equals(to)) {
                c.sendMessage("from " + from.getName() + ": " + msg);
                from.sendMessage("to " + to + " msg: " + msg);
                break;
            }
        }
    }

    public void unSubscribeMe(ClientHandler c) {
        clients.remove(c);
        broadcastUsersList();
    }
}
